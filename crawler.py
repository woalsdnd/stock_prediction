# CAVEATE
# 1. run with python3

import os
from bs4 import BeautifulSoup
import urllib.request

import pandas as pd
import numpy as np

import re
from datetime import datetime
import json
import lxml.html


def makedirs(dir_path):
    """
    Make directories if not exists
    
    Args:
        dir_path (str): path to the directory

    Returns:
        None 
    
    Examples:
        >>> makedirs("tmp")
    """
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)


def convert_to_float(str):
    if str == 'N/A':
        return float('-inf')
    else:
        return float(str.replace(",", ""))


def soaring_stock_codes(percentage_threshold=10):
    """
    get codes of soaring stocks
    
    :percentage_threshold: int above which to collect
    """
    list_codes = []
    # get the last page
    for sosok in range(2):
        page = urllib.request.urlopen('https://finance.naver.com/sise/sise_low_up.nhn?sosok={}'.format(sosok))
        soup = BeautifulSoup(page.read().decode('euc-kr'), "html.parser")
        trs = soup.findAll('tr')
        for tr in trs:
            tds = tr.findAll('td')
            for td in tds:
                if td.find('a'):
                    code = td.find('a')['href'][-6:]
                elif td.find("span") and td.find("span")["class"] == ['tah', 'p11', 'red01']:
                    soar_rate = float(td.find("span").contents[0].strip("\n\t%"))
                    if soar_rate > percentage_threshold:
                        list_codes.append(code)
    return list_codes


def soaring_stock_trading_history(path_outdir="soaring_stock_trading_history"):
    """
    crawl trading history from naver.com 
    
    :path_outdir: str for the path to output directory
    """        
    file_home_path = os.path.dirname(os.path.realpath(__file__))
    makedirs(os.path.join(file_home_path, path_outdir))
    
    list_codes = soaring_stock_codes()
    date = datetime.now().strftime('%Y%m%d') + "1600"
    for code in list_codes:
        chaegyol_time = []
        chaegyol_price = []
        junilbi = []
        maedo = []
        maesu = []
        korae_all = []
        byundong = []
        # get the last page
        page = urllib.request.urlopen('https://finance.naver.com/item/sise_time.nhn?code={}&thistime={}'.format(code, date))
        soup = BeautifulSoup(page.read().decode('euc-kr'), "html.parser")
        tds = soup.findAll('td', {'class' : 'pgRR'})
        final_page_str = tds[0].find('a')['href'][-10:]
        n_pages = int(final_page_str[final_page_str.find("page=") + 5:])
        # enumerate to the last page
        for i in range(1, n_pages + 1):
            page = urllib.request.urlopen('https://finance.naver.com/item/sise_time.nhn?code={}&thistime={}&page={}'.format(code, date, i))
            soup = BeautifulSoup(page.read().decode('euc-kr'), "html.parser")
            trs = soup.findAll('tr', {'onmouseover' : 'mouseOver(this)'})
            for tr in trs:
                tds = tr.findAll('td')
                for index, td in enumerate(tds):
                    if index == 0 and td.find('span'):
                        chaegyol_time.append(td.find('span').contents[0])
                    elif index == 1 and td.find('span'):
                        chaegyol_price.append(td.find('span').contents[0])
                    elif index == 2 and td.find('span'):
                        junilbi.append(td.find('span').contents[0].strip("\n\t"))
                    elif index == 3 and td.find('span'):
                        maedo.append(td.find('span').contents[0])
                    elif index == 4 and td.find('span'):
                        maesu.append(td.find('span').contents[0])
                    elif index == 5 and td.find('span'):
                        korae_all.append(td.find('span').contents[0])
                    elif index == 6 and td.find('span'):
                        byundong.append(td.find('span').contents[0])
            
        # reverse the order
        chaegyol_time = chaegyol_time[::-1]
        chaegyol_price = chaegyol_price[::-1]
        junilbi = junilbi[::-1]
        maedo = maedo[::-1]
        maesu = maesu[::-1]
        korae_all = korae_all[::-1]
        byundong = byundong[::-1]
        
        # save to csv
        df = pd.DataFrame({"chaegyol_time":chaegyol_time, "chaegyol_price":chaegyol_price, "junilbi":junilbi, "maedo":maedo,
                           "maesu":maesu, "korae_all":korae_all, "byundong":byundong})
        df.to_csv(os.path.join(file_home_path, path_outdir, "{}_{}.csv".format(code, date)), index=False)


def stock_trading_history(stock_type):
    """
    crawl trading history from naver.com 
    
    :path_outdir: str for the path to output directory
    """        
    if stock_type == "non_soaring_stocks":
        path_outdir = "non_soaring_stock_trading_history"
    elif stock_type == "all_stocks":
        path_outdir = "all_stock_trading_history"
    else:
        raise ValueError("stock_type should be either non_soaring_stocks or all_stocks")

    file_home_path = os.path.dirname(os.path.realpath(__file__))
    makedirs(os.path.join(file_home_path, path_outdir))
    kospi_code = pd.read_csv(os.path.join(file_home_path, "market_value", "kospi_naver.csv"))["code"].tolist()
    kosdaq_code = pd.read_csv(os.path.join(file_home_path, "market_value", "kosdaq_naver.csv"))["code"].tolist()
    list_codes = kospi_code + kosdaq_code
    list_codes = [code[:-3] for code in list_codes]  # strip .KS or .KQ

    if stock_type == "non_soaring_stocks":
        list_soaring_codes = soaring_stock_codes()
        list_codes = list(set(list_codes) - set(list_soaring_codes))
    
    date = datetime.now().strftime('%Y%m%d') + "1600"
    for code in list_codes:
        chaegyol_time = []
        chaegyol_price = []
        junilbi = []
        maedo = []
        maesu = []
        korae_all = []
        byundong = []
        # get the last page
        page = urllib.request.urlopen('https://finance.naver.com/item/sise_time.nhn?code={}&thistime={}'.format(code, date))
        soup = BeautifulSoup(page.read().decode('euc-kr'), "html.parser")
        tds = soup.findAll('td', {'class' : 'pgRR'})
        if len(tds) == 0:  # stock gone out of the market
            continue
        final_page_str = tds[0].find('a')['href'][-10:]
        n_pages = int(final_page_str[final_page_str.find("page=") + 5:])
        # enumerate to the last page
        for i in range(1, n_pages + 1):
            page = urllib.request.urlopen('https://finance.naver.com/item/sise_time.nhn?code={}&thistime={}&page={}'.format(code, date, i))
            soup = BeautifulSoup(page.read().decode('euc-kr'), "html.parser")
            trs = soup.findAll('tr', {'onmouseover' : 'mouseOver(this)'})
            for tr in trs:
                tds = tr.findAll('td')
                for index, td in enumerate(tds):
                    if index == 0 and td.find('span'):
                        chaegyol_time.append(td.find('span').contents[0])
                    elif index == 1 and td.find('span'):
                        chaegyol_price.append(td.find('span').contents[0])
                    elif index == 2 and td.find('span'):
                        junilbi.append(td.find('span').contents[0].strip("\n\t"))
                    elif index == 3 and td.find('span'):
                        maedo.append(td.find('span').contents[0])
                    elif index == 4 and td.find('span'):
                        maesu.append(td.find('span').contents[0])
                    elif index == 5 and td.find('span'):
                        korae_all.append(td.find('span').contents[0])
                    elif index == 6 and td.find('span'):
                        byundong.append(td.find('span').contents[0])
            
        # reverse the order
        chaegyol_time = chaegyol_time[::-1]
        chaegyol_price = chaegyol_price[::-1]
        junilbi = junilbi[::-1]
        maedo = maedo[::-1]
        maesu = maesu[::-1]
        korae_all = korae_all[::-1]
        byundong = byundong[::-1]
        
        # save to csv
        df = pd.DataFrame({"chaegyol_time":chaegyol_time, "chaegyol_price":chaegyol_price, "junilbi":junilbi, "maedo":maedo,
                           "maesu":maesu, "korae_all":korae_all, "byundong":byundong})
        df.to_csv(os.path.join(file_home_path, path_outdir, "{}_{}.csv".format(code, date)), index=False)


def stock_trading_history_realtime(code, date, path_history_for_inference):
    """
    crawl trading history from naver.com in real time 
    
    :code: str for zongmok code
    :path_history_for_inference: str for path to directory for history for inference
    """        
    csv_path = os.path.join(path_history_for_inference, "{}_{}.csv".format(code, date))
    update = os.path.exists(csv_path)
    chaegyol_time = []
    chaegyol_price = []
    junilbi = []
    maedo = []
    maesu = []
    korae_all = []
    byundong = []
    
    if update:
        df = pd.read_csv(csv_path)
        prev_chaegyol_time = list(df["chaegyol_time"])
        prev_chaegyol_price = list(df["chaegyol_price"])
        prev_junilbi = list(df["junilbi"])
        prev_maedo = list(df["maedo"])
        prev_maesu = list(df["maesu"])
        prev_korae_all = list(df["korae_all"])
        prev_byundong = list(df["byundong"])
    
    page = urllib.request.urlopen('https://finance.naver.com/item/sise_time.nhn?code={}&thistime={}&page=1'.format(code, date))
    soup = BeautifulSoup(page.read().decode('euc-kr'), "html.parser")
    trs = soup.findAll('tr', {'onmouseover' : 'mouseOver(this)'})
    retrieved = False
    for tr in trs:
        if retrieved:
            break
        tds = tr.findAll('td')
        for index, td in enumerate(tds):
            if index == 0 and td.find('span'):
                val = td.find('span').contents[0]
                if not update or (update and val not in prev_chaegyol_time):
                    chaegyol_time.append(td.find('span').contents[0])
                else:
                    retrieved = True
                    break
            elif index == 1 and td.find('span'):
                chaegyol_price.append(td.find('span').contents[0])
            elif index == 2 and td.find('span'):
                junilbi.append(td.find('span').contents[0].strip("\n\t"))
            elif index == 3 and td.find('span'):
                maedo.append(td.find('span').contents[0])
            elif index == 4 and td.find('span'):
                maesu.append(td.find('span').contents[0])
            elif index == 5 and td.find('span'):
                korae_all.append(td.find('span').contents[0])
            elif index == 6 and td.find('span'):
                byundong.append(td.find('span').contents[0])
    
    # reverse the order
    chaegyol_time = chaegyol_time[::-1]
    chaegyol_price = chaegyol_price[::-1]
    junilbi = junilbi[::-1]
    maedo = maedo[::-1]
    maesu = maesu[::-1]
    korae_all = korae_all[::-1]
    byundong = byundong[::-1]
    
    if update:
        chaegyol_time = prev_chaegyol_time + chaegyol_time
        chaegyol_price = prev_chaegyol_price + chaegyol_price
        junilbi = prev_junilbi + junilbi 
        maedo = prev_maedo + maedo
        maesu = prev_maesu + maesu
        korae_all = prev_korae_all + korae_all
        byundong = prev_byundong + byundong
    
    # save to csv
    # cut the last element as it is incomplete
    df = pd.DataFrame({"chaegyol_time":chaegyol_time[:-1], "chaegyol_price":chaegyol_price[:-1], "junilbi":junilbi[:-1], "maedo":maedo[:-1],
                       "maesu":maesu[:-1], "korae_all":korae_all[:-1], "byundong":byundong[:-1]})
    df.to_csv(csv_path, index=False)


def market_value(path_outdir="market_value"):
    """
    crawl market values from naver.com 
    
    :path_outdir: str for the path to output directory
    """        
    for index_type, stype in enumerate(["kospi", "kosdaq"]):
        stock_code_list = list()
        stock_name_list = list()
        stock_price_list = list()
        stock_return_list = list()    
        stock_market_capitalization_list = list()
        stock_n_shares_list = list()
        stock_foreigner_list = list()
        stock_volume_list = list()
        stock_per_list = list()
        stock_roe_list = list()
        
        # get the last page
        page = urllib.request.urlopen('http://finance.naver.com/sise/sise_market_sum.nhn?sosok={}'.format(index_type))
        soup = BeautifulSoup(page.read().decode('euc-kr'), "html.parser")
        tds = soup.findAll('td', {'class' : 'pgRR'})
        if len(tds) == 0:  # stock gone out of the market
            continue
        final_page_str = tds[0].find('a')['href'][-10:]
        n_pages = int(final_page_str[final_page_str.find("page=") + 5:])
        for i in range(1, n_pages + 1):
            page = urllib.request.urlopen('http://finance.naver.com/sise/sise_market_sum.nhn?sosok={}&page={}'.format(index_type, i))
            soup = BeautifulSoup(page.read().decode('euc-kr'), "html.parser")
            trs = soup.findAll('tr', {'onmouseover' : 'mouseOver(this)'})
            for tr in trs:
                tds = tr.findAll('td')
                for index, td in enumerate(tds):
                    # skip ETF (PER is N/A)
                    if tds[10].contents[0] == "N/A":
                        continue
                    # store info into lists
                    if index == 1:
                        stock_code_list.append(td.find('a')['href'][-6:] + ".KS" if stype == "kospi" else td.find('a')['href'][-6:] + ".KQ")
                        stock_name_list.append(td.find('a').contents[0])
                    elif index == 2:
                        stock_price_list.append(int(td.contents[0].replace(",", "")))
                    elif index == 4:
                        stock_return_list.append(float(td.find('span').contents[0].replace("%", "")))
                    elif index == 6:
                        stock_market_capitalization_list.append(int(td.contents[0].replace(",", "")))
                    elif index == 7:
                        stock_n_shares_list.append(int(td.contents[0].replace(",", "")))
                    elif index == 8:
                        stock_foreigner_list.append(convert_to_float(td.contents[0]))
                    elif index == 9:
                        stock_volume_list.append(int(td.contents[0].replace(",", "")))
                    elif index == 10:
                        stock_per_list.append(convert_to_float(td.contents[0]))
                    elif index == 11:
                        stock_roe_list.append(convert_to_float(td.contents[0]))
                    
        df = pd.DataFrame({"code":stock_code_list, "name":stock_name_list, "price":stock_price_list, "return":stock_return_list,
                           "market_capitalization":stock_market_capitalization_list, "n_shares":stock_n_shares_list,
                           "foreigner":stock_foreigner_list, "volume":stock_volume_list, "PER":stock_per_list, "ROE":stock_roe_list})
        file_home_path = os.path.dirname(os.path.realpath(__file__))
        df.to_csv(os.path.join(file_home_path, path_outdir, stype + "_naver.csv"), index=False)


def financial_statement(path_outdir="fundamental"):
    """
    crawl financial statements from naver.com 

    :path_outdir: str for the path to output directory

    """
    file_home_path = os.path.dirname(os.path.realpath(__file__))
    kospi_csv, kosdaq_csv = os.path.join(file_home_path, "market_value", "kospi_naver.csv"), os.path.join(file_home_path, "market_value", "kosdaq_naver.csv")
    list_kospi, list_kosdaq = pd.read_csv(kospi_csv).code.tolist(), pd.read_csv(kosdaq_csv).code.tolist()
    
    apikey = "23fb6282c8e91fb8c60cc68a44f886444208351b"
    url_template = "http://dart.fss.or.kr/api/search.json?auth={}".format(apikey) + "&crp_cd={}&start_dt=20000101"
    
    for code_type in list_kospi + list_kosdaq:
        code = code_type.split(".")[0]
        url_gaehwang = url_template.format(code)
        response = urllib.request.urlopen(url_gaehwang)
        data = json.load(response)
        for dict_item in data["list"]:
            if "보고서" in dict_item["rpt_nm"]:
                rcp_no = dict_item["rcp_no"]
                url_gongsi = "http://dart.fss.or.kr/dsaf001/main.do?rcpNo={}".format(rcp_no)
                req = urllib.request.urlopen(url_gongsi).read()
                tree = lxml.html.fromstring(req)
                onclick = tree.xpath('//*[@id="north"]/div[2]/ul/li[1]/a')[0].attrib['onclick']
                pattern = re.compile("^openPdfDownload\('\d+',\s*'(\d+)'\)")
                dcm_no = pattern.search(onclick).group(1)
                
                download_url = "http://dart.fss.or.kr/pdf/download/excel.do?lang=ko&rcp_no={}&dcm_no={}".format(rcp_no, dcm_no)
                urllib.request.urlretrieve(download_url, os.path.join(path_outdir, "{}.xls".format(code)))


def save_stock_to_file(path_outdir, code, start_date):
    """
    download the series of stock price for the given code (retry every second)
    
    :path_outdir: str for the path to output directory
    """
    filename = os.path.join(path_outdir, str(code))
    while rerun:
        if os.path.exists(filename):
            df_file = pd.read_csv(filename)
            if not df_file.empty:
                break
        df = data.get_data_yahoo(code, start_date)
        df.to_csv(filename)
        time.sleep(1)

