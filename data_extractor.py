import pandas as pd
from datetime import datetime, timedelta
import numpy as np
import os
import data_organizer


def makedirs(dir_path):
    """
    Make directories if not exists
    """
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)

        
def korae_all(path_outdir="korae_all"):
    """
    average korae ryang for the given days and save the results to the designated path
    """
    START_DATE = datetime(2018, 10, 25, 0, 0)
    
    file_home_path = os.path.dirname(os.path.realpath(__file__))
    kospi_csv, kosdaq_csv = os.path.join(file_home_path, "market_value", "kospi_naver.csv"), os.path.join(file_home_path, "market_value", "kosdaq_naver.csv")
    list_kospi, list_kosdaq = pd.read_csv(kospi_csv).code.tolist(), pd.read_csv(kosdaq_csv).code.tolist()
    all_history_home = os.path.join(file_home_path, "all_stock_trading_history")
    makedirs(os.path.join(file_home_path, path_outdir))
    
    for code_type in list_kospi + list_kosdaq:
        code = code_type.split(".")[0]
        date = datetime.now()
        list_korae = []
        list_date = []
        for days_back in range((date - START_DATE).days + 1):
            date = (datetime.now() - timedelta(days=days_back))
            str_date = date.strftime('%Y%m%d') + "1600"
            fid = "{}_{}.csv".format(code, str_date)
            fpath = os.path.join(all_history_home, fid)
            if os.path.exists(fpath):
                df = data_organizer.load_csv(fpath)
                list_korae.append(df["korae_all"].tolist()[-1])
                list_date.append(str_date)
        
        pd.DataFrame({"korae_all":list_korae, "date":list_date}).to_csv(os.path.join(file_home_path, path_outdir, "{}.csv".format(code)), index=False)

