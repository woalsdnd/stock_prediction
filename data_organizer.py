import pandas as pd
from datetime import datetime
import numpy as np
import os
import time

WINDOW_KORAE = 5


def misc_data(code, date, data_type):
    """
    return misc data for the given zongmok (specified by code)
    
    :code: zongmok code (str)
    :date: date (int) EX) 201811061600
    :data_type: data type to extract
    """
    list_data = []
    for dt in data_type:
        if dt == "korae_all":
            file_home_path = os.path.dirname(os.path.realpath(__file__))
            fpath = os.path.join(file_home_path, "korae_all", "{}.csv".format(code))
            df = pd.read_csv(fpath)
            df_prev = df[df.date < date] if len(df[df.date < date]) > 1 else df[df.date == date]
            df_prev_sorted_by_date = df_prev.sort_values(by="date")
            effective_koraerayng = df_prev_sorted_by_date["korae_all"].tolist()[-WINDOW_KORAE:]
            if len(effective_koraerayng) != 0:
                list_data.append(np.mean(effective_koraerayng))
            else:
                return None
    return list_data


def load_csv(csv_file_path):
    """
    return dataframe
    (forward-fill chaegyol_price, junilbi, maedo, maesu, korae_all and assign 0 to byundong)
    
    :csv_file_path: str
    """
    df_crawl = pd.read_csv(csv_file_path)
    df_crawl.set_index("chaegyol_time", inplace=True)
    list_time = ["{:02d}:{:02d}".format(h, m) for h in range(9, 16) for m in range(0, 60)]
    df = pd.DataFrame(index=list_time, columns=df_crawl.columns)
    df.update(df_crawl)
    df.loc[pd.isna(df.byundong), "byundong"] = 0
    df.loc[:, "chaegyol_price"] = df["chaegyol_price"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    df.loc[:, "junilbi"] = df["junilbi"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    df.loc[:, "maedo"] = df["maedo"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    df.loc[:, "maesu"] = df["maesu"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    # if maesu and maedo is 0, convert it to None
    df.loc[:, "maedo"] = df["maedo"].map(lambda x:None if x == 0 else x)
    df.loc[:, "maesu"] = df["maesu"].map(lambda x:None if x == 0 else x)
    df.loc[:, "korae_all"] = df["korae_all"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    df.loc[:, "byundong"] = df["byundong"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    
    # fill NA values
    df.fillna(method="ffill", inplace=True)
    df.fillna(method="bfill", inplace=True)  # in case of no chaegyol at 09:00
    df.fillna(0, inplace=True)  # in case no maesu or maedo exist 
    return df


def load_csv_inference(csv_file_path):
    """
    return dataframe or None (if empty)
    (forward-fill chaegyol_price, junilbi, maedo, maesu, korae_all and assign 0 to byundong)
    
    :csv_file_path: str
    """
    while True:
        try:
            df_crawl = pd.read_csv(csv_file_path)
            break
        except:
            time.sleep(1)
            df_crawl = pd.read_csv(csv_file_path)
            break
            
    if len(df_crawl) == 0:
        return None
    start_hour, start_min = [int(el) for el in np.min(df_crawl["chaegyol_time"]).split(":")]
    end_hour, end_min = [int(el) for el in np.max(df_crawl["chaegyol_time"]).split(":")]
    list_time = [] 
    curr_hour, curr_min = start_hour, start_min
    while not (curr_hour == end_hour and curr_min == end_min):
        list_time.append("{:02}:{:02}".format(curr_hour, curr_min))
        curr_min += 1
        if curr_min == 60:
            curr_min = 0
            curr_hour += 1
    df_crawl.set_index("chaegyol_time", inplace=True)
    df = pd.DataFrame(index=list_time, columns=df_crawl.columns)
    df.update(df_crawl)
    df.loc[pd.isna(df.byundong), "byundong"] = 0
    df.loc[:, "chaegyol_price"] = df["chaegyol_price"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    df.loc[:, "junilbi"] = df["junilbi"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    df.loc[:, "maedo"] = df["maedo"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    df.loc[:, "maesu"] = df["maesu"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    # if maesu and maedo is 0, convert it to None
    df.loc[:, "maedo"] = df["maedo"].map(lambda x:None if x == 0 else x)
    df.loc[:, "maesu"] = df["maesu"].map(lambda x:None if x == 0 else x)
    df.loc[:, "korae_all"] = df["korae_all"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    df.loc[:, "byundong"] = df["byundong"].map(lambda x:int(x.replace(",", "")) if type(x) == str else x)
    
    # fill NA values
    df.fillna(method="ffill", inplace=True)
    df.fillna(method="bfill", inplace=True)  # in case of no chaegyol at 09:00
    df.fillna(0, inplace=True)  # in case no maesu or maedo exist
    return df


def split_dataset(inputs, outputs, val_ratio=0.1):
    """
    return training_inputs, training_labels, validation_inputs, validation_labels
    indices are randomly selected
    
    :inputs: matrix [samples, time steps, features]
    :outputs: array-like [0|1] 
    """
    assert len(inputs) == len(outputs)
    N = len(inputs)
    N_val = int(val_ratio * N)
    indices_val = np.random.choice(N, N_val, replace=False)
    indices_training = list(set(range(N)) - set(indices_val))
    return inputs[indices_training], outputs[indices_training], inputs[indices_val], outputs[indices_val]
    

def beginning_sequence(non_soaring_periods, soaring_periods, input_len):
    """
    return beginning sequence of inputs and binary outputs (soaring or not)   
    inputs: [samples, time steps, features]
    outputs: [0|1]*#samples 0: non-soaring, 1:soaring

    :non_soaring_periods: list of matrices where each matrix is a time sequence data (period)
    :soaring_periods: list of matrices where each matrix is a time sequence data (period)
    :input_len: int
    """
    inputs = []
    list_labels = [0] * len(non_soaring_periods) + [1] * len(soaring_periods)
    periods = non_soaring_periods + soaring_periods
    for index in range(len(periods)):
        begin_seq = periods[index][:input_len, :]
        if len(begin_seq) == input_len:
            inputs.append(begin_seq)
    
    inputs = np.array(inputs)
    outputs = np.array(list_labels)
    return inputs, outputs
    
 
def normalize(list_periods, misc_data):
    """
    return normalized inputs for chaegyol_price, junilbi, maedo, maesu, korae_all, byundong
    [chaegyol_price/junil_price, maedo/junil_price, maesu/junil_price, byundong/(average koraeryang)]
     
    CAVEATE: 
    1. junilbi is an absolute value junil_price should be computed
    2. period should be longer than 2        
    
    :list_periods: list of matrices where each matrix is a time sequence data (period)
    :misc_data: list of misc data
    """
    list_normalized_periods = []
    if misc_data is None:  # if misc data is None, return an empty list
        return None
    average_koraeryang, = misc_data
    for period in list_periods:
        # guess the sign of junilbi
        p1, delta1 = period[0, 0], period[0, 1]
        for i in range(len(period)):
            p2, delta2 = period[i, 0], period[i, 1] 
            if p2 != p1:
                break
        if p1 + delta1 == p2 + delta2 or p1 + delta1 == p2 - delta2:
            junil_price = p1 + delta1
        elif p1 - delta1 == p2 + delta2 or p1 - delta1 == p2 - delta2:
            junil_price = p1 - delta1
        else:
            raise ValueError("junil_price could not be computed")
        # normalize the input
        epsilon = 1e-5
        normalized_prices = 1.*period[:, [0, 2, 3]] / junil_price
        normalized_koraerayng = np.log10(1 + period[:, 5] / (average_koraeryang / 420))
        normalized_period = np.concatenate([normalized_prices, np.expand_dims(normalized_koraerayng, axis=1)], axis=1)
        list_normalized_periods.append(normalized_period)

    return list_normalized_periods


def split_periods(df, **kwargs):
    """
    return list of matrices where each matrix is a time sequence data (period)
    
    :df: dataframe
    
    *** kwargs ***
    :min_len: minimum length for the period
    :period_len: length of the period 
    :input_len: length of the input for the network
    :tolerance: tolerate until the price hits highest_price+(highest_price-start_price)*tolerance
    :gain: profit to be soaring period
    """
    list_non_soaring_periods_indices, list_soaring_periods_indices = split_periods_indices_v4(df, kwargs["period_len"], kwargs["input_len"], kwargs["tolerance"], kwargs["gain"])
    list_non_soaring_periods = [df.iloc[period].values for period in list_non_soaring_periods_indices]
    list_soaring_periods = [df.iloc[period].values for period in list_soaring_periods_indices]
    return list_non_soaring_periods, list_soaring_periods


def split_periods_indices_v4(df, period_len, input_len, tolerance=-0.01, gain=0.05):
    """
    return the soaring/non-soaring periods
    
    :df: dataframe 
    :period_len: length of the period (v1)
    :input_len: length of the input for the network (v1)
    :gain: minimum increase for the period to be soaring
    """
    list_soaring_periods, list_non_soaring_periods = [], []
    list_chaegyol_price = df["chaegyol_price"].tolist()
        
    # collect soaring periods
    init_price = list_chaegyol_price[0]
    prev_index = 0
    for index in range(len(list_chaegyol_price) - period_len):
        max_head = np.max(list_chaegyol_price[index:index + input_len])
        max_tail = np.max(list_chaegyol_price[index + input_len:index + period_len])
        min_price = np.min(list_chaegyol_price[index + input_len:index + period_len])
        if max_head * gain < max_tail - max_head and (1 + tolerance) * list_chaegyol_price[index + input_len] < min_price:
            list_soaring_periods.append(range(index, index + period_len))
        elif list_chaegyol_price[index + input_len] >= min_price and max_head > max_tail:  # price goes down 
            if index > prev_index + input_len // 4:
                prev_index = index
                list_non_soaring_periods.append(range(index, index + period_len))
                
    return list_non_soaring_periods, list_soaring_periods

