import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
import data_organizer
import re

LIST_GAIN = [0.1, 0.15, 0.2, 0.25, 0.3]


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# load data
# fpaths = all_files_under("soaring_stock_trading_history")
# fpaths = all_files_under("all_stock_trading_history")
fpaths = all_files_under("history_for_inference")
for gain in LIST_GAIN:
    list_duration = []
    for fpath in fpaths:
        df = data_organizer.load_csv(fpath)
        mat = df.as_matrix(columns=df.columns)
        mat_9to10 = mat[:60, :]
        max_price = np.max(mat_9to10[:, 0])
        duration = np.argmax(mat_9to10[:, 0])
        if max_price - mat_9to10[0, 0] > gain * mat_9to10[0, 0]:
            list_duration.append(duration)
    #         plt.scatter(range(len(mat_9to10)), mat_9to10[:, 0] * mat_9to10[:, 5], s=5) 
    
    print "gain: [{}] => total count:{}, duration: {} ({}), min: {}, max: {} ".format(gain, len(list_duration), np.mean(list_duration), np.std(list_duration), np.min(list_duration), np.max(list_duration))
# plt.xlabel("time")
# plt.ylim(0, 5e8)
# plt.ylabel("total price")
# plt.savefig("koraerayng_soaring_9to10.png")
