# python train.py --gpu_index=3 --period_len=240 --input_len=60 --load_model_dir=network_models/240_60/ --from_date=201901281600 --lr=1e-5 
import os 
import re
import datetime
import sys
import argparse

from sklearn.metrics.ranking import roc_auc_score
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils import class_weight
from keras.models import model_from_json

import models
import data_organizer
import data_extractor

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=int,
    help="GPU index",
    required=True
    )
parser.add_argument(
    '--period_len',
    type=int,
    required=True
    )
parser.add_argument(
    '--input_len',
    type=int,
    required=True
    )
parser.add_argument(
    '--load_model_dir',
    type=str,
    required=False,
    default=None
    )
parser.add_argument(
    '--gain',
    type=float,
    required=False,
    default=0.05
    )
parser.add_argument(
    '--tolerance',
    type=float,
    required=False,
    default=-0.01
    )
parser.add_argument(
    '--is_9to10',
    type=bool,
    required=False,
    default=False
    )
parser.add_argument(
    '--from_date',
    type=int,
    required=False,
    default=False
    )
parser.add_argument(
    '--lr',
    type=float,
    required=False,
    default=None
    )
FLAGS, _ = parser.parse_known_args()

GPU_INDEX = FLAGS.gpu_index
PERIOD_LEN = FLAGS.period_len
INPUT_LEN = FLAGS.input_len
GAIN = FLAGS.gain
TOLERANCE = FLAGS.tolerance
N_EPOCHS = 10
BATCH_SIZE = 4
VAL_RATIO = 0.1
MODEL_DIR = "trained_models"

os.environ['CUDA_VISIBLE_DEVICES'] = str(GPU_INDEX)


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)


def binary_stats(y_true, y_pred):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
    prec = 1.*cm[1, 1] / (cm[0, 1] + cm[1, 1]) if cm[0, 1] + cm[1, 1] != 0 else 0
    acc = 1.*np.trace(cm) / np.sum(cm)
    sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
    AUC_ROC = roc_auc_score(y_true, y_pred)
    return prec, acc, sen, AUC_ROC


# convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=1):
    dataX, dataY = [], []
    for i in range(len(dataset) - look_back - 1):
        a = dataset[i:(i + look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
    return numpy.array(dataX), numpy.array(dataY)


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# update koraeryang if korae_all files are outdated
koraeall_files = all_files_under("korae_all")
if len(koraeall_files) > 0:
    modified_date = np.max([int(str(datetime.datetime.fromtimestamp(os.path.getmtime(koraeall_file))).split(" ")[0].replace("-", "")) for koraeall_file in koraeall_files])
    today = int(str(datetime.datetime.today()).split(" ")[0].replace("-", ""))
    if datetime.datetime.today().weekday() <= 4 and modified_date < today:
        data_extractor.korae_all()        
else:
    data_extractor.korae_all()

# load & normalize data
fpaths = all_files_under("all_stock_trading_history")
if FLAGS.from_date:
    fpaths = filter(lambda x: int(os.path.basename(x).split("_")[1].replace(".csv", "")) >= FLAGS.from_date, fpaths)
non_soaring_periods_train, soaring_periods_train, non_soaring_periods_val, soaring_periods_val = [], [], [], []
for fpath in fpaths:
    df = data_organizer.load_csv(fpath)
    if FLAGS.is_9to10:
        df = df.iloc[:60]
    code, tail = os.path.basename(fpath).split("_") 
    date = re.sub(".csv$", "", tail)
    misc_data = data_organizer.misc_data(code=code, date=int(date), data_type=["korae_all"])
    list_non_soaring_periods, list_soaring_periods = data_organizer.split_periods(df, period_len=PERIOD_LEN, input_len=INPUT_LEN, tolerance=TOLERANCE, gain=GAIN)
    list_non_soaring_periods_normalized = data_organizer.normalize(list_non_soaring_periods, misc_data)
    list_soaring_periods_normalized = data_organizer.normalize(list_soaring_periods, misc_data)
    # include to train or validation set
    if np.random.random() < VAL_RATIO:
        non_soaring_periods_val += list_non_soaring_periods_normalized
        soaring_periods_val += list_soaring_periods_normalized
    else:
        non_soaring_periods_train += list_non_soaring_periods_normalized
        soaring_periods_train += list_soaring_periods_normalized

# print and visualize the number of data and inspect inputs
non_soaring_periods = non_soaring_periods_train + non_soaring_periods_val
soaring_periods = soaring_periods_train + soaring_periods_val
print("non-soaring periods: {} (train: {}, val: {}), soaring periods: {} (train: {}, val: {})".format(len(non_soaring_periods), len(non_soaring_periods_train), len(non_soaring_periods_val), len(soaring_periods), len(soaring_periods_train), len(soaring_periods_val)))
plt.close()
for index_soaring in np.random.choice(range(len(soaring_periods_train)), 10):
    plt.plot(range(len(soaring_periods_train[index_soaring][:, 0])), soaring_periods_train[index_soaring][:, 0], color="r")
for index_non_soaring in np.random.choice(range(len(non_soaring_periods_train)), 10):
    plt.plot(range(len(non_soaring_periods_train[index_non_soaring][:, 0])), non_soaring_periods_train[index_non_soaring][ :, 0], color="b")
plt.xlabel("minutes")
plt.ylabel("price")
plt.savefig(os.path.join("input_checks", "periods_sample_{}_{}.png".format(PERIOD_LEN, INPUT_LEN)))

# reshape input to be [samples, time steps, features]
inputs_train, labels_train = data_organizer.beginning_sequence(non_soaring_periods_train, soaring_periods_train, INPUT_LEN)
inputs_val, labels_val = data_organizer.beginning_sequence(non_soaring_periods_val, soaring_periods_val, INPUT_LEN)

# create (or load) an LSTM network
if FLAGS.load_model_dir:
    network_file = all_files_under(FLAGS.load_model_dir, extension=".json")
    weight_file = all_files_under(FLAGS.load_model_dir, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    if FLAGS.lr:
        network = models.set_optimizer(network, FLAGS.lr)
    else:
        network = models.set_optimizer(network)
    print("model loaded from files")
else:
    network = models.bidirectional_LSTM(inputs_train[0].shape)
    print("model from the scratch")
# save the network
with open(os.path.join(MODEL_DIR, "network_{}_{}.json".format(PERIOD_LEN, INPUT_LEN)), 'w') as f:
    f.write(network.to_json())
network.summary()

# training loop
class_weight = class_weight.compute_class_weight('balanced', np.unique(labels_train), labels_train)
for n_epoch in range(N_EPOCHS):
    network.fit(inputs_train, labels_train, epochs=1, batch_size=BATCH_SIZE, verbose=0, shuffle=True, class_weight=class_weight)
    # make predictions
    val_pred = network.predict(inputs_val, batch_size=1)
    
    # calculate accuracy
    val_prec, val_acc, val_sen, val_AUC_ROC = binary_stats(labels_val, val_pred)
    print("{} epoch - Validation prec: {}, acc: {}, sen: {}, AUROC: {}".format(n_epoch, val_prec, val_acc, val_sen, val_AUC_ROC))
    sys.stdout.flush()

    # save network weights    
    network.save_weights(os.path.join(MODEL_DIR, "weight_{}_{}_{}epoch.h5".format(PERIOD_LEN, INPUT_LEN, n_epoch)))
