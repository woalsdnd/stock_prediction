import sys
import os
from PyQt5.QtWidgets import *
from PyQt5.QAxContainer import *
from PyQt5.QtCore import *
import time
import pandas as pd

TR_REQ_TIME_INTERVAL = 0.2

class Kiwoom(QAxWidget):
    def __init__(self):
        super().__init__()
        self._create_kiwoom_instance()
        self._set_signal_slots()
        self.real_data_buffer = {}
        self.michaegyol_zongmok = {}
        self.jango = None
        
    def _create_kiwoom_instance(self):
        self.setControl("KHOPENAPI.KHOpenAPICtrl.1")

    def _set_signal_slots(self):
        self.OnEventConnect.connect(self._event_connect)
        self.OnReceiveTrData.connect(self._receive_tr_data)
        self.OnReceiveRealData.connect(self._receive_real_data)
        self.OnReceiveChejanData.connect(self._receive_chejan_data)

    def comm_rq_data(self, rqname, trcode, next, screen_no):
        self.dynamicCall("CommRqData(QString, QString, int, QString)", rqname, trcode, next, screen_no)
        self.tr_event_loop = QEventLoop()
        self.tr_event_loop.exec_()
        
    def comm_connect(self):
        self.dynamicCall("CommConnect()")
        self.login_event_loop = QEventLoop()
        self.login_event_loop.exec_()

    def _event_connect(self, err_code):
        if err_code == 0:
            print("connected")
        else:
            print("disconnected")
        self.login_event_loop.exit()

    def get_connect_state(self):
        ret = self.dynamicCall("GetConnectState()")
        return ret

    def _get_repeat_cnt(self, trcode, record_name):
        ret = self.dynamicCall("GetRepeatCnt(QString, QString)", trcode, record_name)
        return ret

    def set_input_value(self, _id, _value):
        self.dynamicCall("SetInputValue(QString, QString)",_id, _value)

    def _get_comm_data(self, trcode, record_name, index, item_name):
        ret = self.dynamicCall("GetCommData(QString, QString, int, QString)", trcode, record_name, index, item_name)
        return ret.strip()

    def _get_comm_realdata(self, code, fid):
        ret = self.dynamicCall("GetCommRealData(QString, int)", code, fid)
        return ret.strip()

    def _get_chejan_data(self, fid):
        ret = self.dynamicCall("GetChejanData(int)", fid)
        return ret.strip()

    def _receive_chejan_data(self, gubun, item_count, fid_list):
         """
        :param gubun: string - 체결구분('0': 주문접수/주문체결, '1': 잔고통보, '3': 특이신호)
        :param item_count: int - fid의 갯수
        :param fid_list: string - fidList 구분은 ;(세미콜론) 이다.

        """
        if gubun == "0":
            zongmok_code = self._get_chejan_data(9201)
            n_michaegyol = self._get_chejan_data(902)
            org_order_no = self._get_chejan_data(904)
            if int(n_michaegyol) > 0:
                self.michaegyol_zongmok["list_zongmok_code"].append(zongmok_code)
                self.michaegyol_zongmok["list_michaegyol"].append(n_michaegyol)
                self.michaegyol_zongmok["list_org_order_no"].append(org_order_no)
            else:
                if zongmok_code in self.michaegyol_zongmok["list_zongmok_code"]:
                    index = self.michaegyol_zongmok["list_zongmok_code"].index(zongmok_code)
                    self.michaegyol_zongmok["list_zongmok_code"].pop(index)        
                    self.michaegyol_zongmok["list_michaegyol"].pop(index)        
                    self.michaegyol_zongmok["list_org_order_no"].pop(index)        
        elif gubun == "1":
            self.jango = self._get_chejan_data(951)
            list_holding_zongmok = self._get_chejan_data(9001).split(";")
            list_quantity = self._get_chejan_data(903).split(";")
            self.holding_zongmok = {"list_zongmok_code":list_zongmok_code, "list_quantity":list_quantity}
            
    def _receive_real_data(self, code, real_type, real_data):
        print("realdata")    
        print(real_data)
        if real_type == "주식우선호가":
            choiwoosun_maedo = self._get_comm_realdata(code, 27)
            choiwoosun_maesoo = self._get_comm_realdata(code, 28)
            self.real_data_buffer[code].update({"choiwoosun_maedo": choiwoosun_maedo,
                                           "choiwoosun_maesoo":choiwoosun_maesoo}) 
        elif real_type == "주식호가잔량":
            list_maedo = [self._get_comm_realdata(code, fid) for fid in range(41,51)]
            list_maedo_janryang = [self._get_comm_realdata(code, fid) for fid in range(61,71)]
            list_maesoo = [self._get_comm_realdata(code, fid) for fid in range(51,61)]
            list_maesoo_janryang = [self._get_comm_realdata(code, fid) for fid in range(71,81)]
            self.real_data_buffer[code].update({"list_maedo":list_maedo, "list_maedo_janryang":list_maedo_janryang,
                         "list_maesoo":list_maesoo, "list_maesoo_janryang":list_maesoo_janryang})

    def set_real_reg(self, screen_no, codes, fids, real_reg_type):
        """
        :param codes: string - 종목코드 리스트(종목코드;종목코드;...)
        :param fids: string - fid 리스트(fid;fid;...)
        :param realRegType: string - 실시간등록타입(0: 첫 등록, 1: 추가 등록)
        """
        self.dynamicCall("SetRealReg(QString, QString, QString, QString)", screen_no, codes, fids, real_reg_type)

    def set_real_remove(self, screen_no, code):
         self.dynamicCall("SetRealRemove(QString, QString)", screen_no, code)        

    def _receive_tr_data(self, screen_no, rqname, trcode, record_name, next, unused1, unused2, unused3, unused4):
        if rqname == "현재호가":
            self._current_hoga(rqname, trcode)
        elif rqname == "계좌평가잔고내역요청":
            self._jango(rqname, trcode)
        elif rqname == "실시간미체결요청":
            self._michaegyol(rqname, trcode)
        elif rqname == "계좌수익률요청":
            self._holding_zongmok(rqname, trcode)
        else:
            pass
        self.tr_event_loop.exit()

    def _holding_zongmok(self, rqname, trcode):
        list_zongmok_code, list_quantity, list_maeip = [], [], []
        n_items = self._get_repeat_cnt(trcode, "종목코드")
        for index in range(n_items):
            list_zongmok_code.append(self._get_comm_data(trcode, rqname, index, "종목코드"))
            list_quantity.append(self._get_comm_data(trcode, rqname, index, "보유수량"))
            list_maeip.append(self._get_comm_data(trcode, rqname, index, "매입가"))
        self.buffer = {"list_zongmok_code": list_zongmok_code, "list_quantity": list_quantity,
                       "list_maeip":list_maeip}

    def _michaegyol(self, rqname, trcode):
        list_zongmok_code, list_quantity, list_price, list_michaegyol, list_org_order_no = [], [], [], [], []
        n_items=self._get_repeat_cnt(trcode, "미체결수량")
        for index in range(n_items):
            list_zongmok_code.append(self._get_comm_data(trcode, rqname, index, "종목코드"))
            list_quantity.append(self._get_comm_data(trcode, rqname, index, "주문수량"))
            list_price.append(self._get_comm_data(trcode, rqname, index, "주문가격"))
            list_michaegyol.append(self._get_comm_data(trcode, rqname, index, "미체결수량"))
            list_org_order_no.append(self._get_comm_data(trcode, rqname, index, "원주문번호"))
        self.buffer = {"list_zongmok_code": list_zongmok_code, "list_quantity": list_quantity,"list_price":list_price,
                       "list_michaegyol":list_michaegyol, "list_org_order_no":list_org_order_no}

    def _jango(self, rqname, trcode):
        self.buffer=self._get_comm_data(trcode, rqname, 0, "예수금")

    def _current_hoga(self, rqname, trcode):
        list_maedo=[self._get_comm_data(trcode, rqname, 0, "매도최우선호가")]
        list_maedo_janryang=[self._get_comm_data(trcode, rqname, 0, "매도최우선잔량")]
        list_maesoo = [self._get_comm_data(trcode, rqname, 0, "매수최우선호가")]
        list_maesoo_janryang = [self._get_comm_data(trcode, rqname, 0, "매수최우선잔량")]
        for index in range(2,11):
            list_maedo.append(self._get_comm_data(trcode, rqname, 0, "매도{}차선호가".format(index)))
            if index==6:
                list_maedo_janryang.append(self._get_comm_data(trcode, rqname, 0, "매도{}우선잔량".format(index)))
            else:
                list_maedo_janryang.append(self._get_comm_data(trcode, rqname, 0, "매도{}차선잔량".format(index)))
            if index == 6:
                list_maesoo.append(self._get_comm_data(trcode, rqname, 0, "매수{}우선호가".format(index)))
                list_maesoo_janryang.append(self._get_comm_data(trcode, rqname, 0, "매수{}우선잔량".format(index)))
            else:
                list_maesoo.append(self._get_comm_data(trcode, rqname, 0, "매수{}차선호가".format(index)))
                list_maesoo_janryang.append(self._get_comm_data(trcode, rqname, 0, "매수{}차선잔량".format(index)))
        self.buffer={"list_maedo":list_maedo, "list_maedo_janryang":list_maedo_janryang,
                     "list_maesoo":list_maesoo, "list_maesoo_janryang":list_maesoo_janryang}

    def send_order(self, rqname, screen_no, account_no, order_type, zongmok_code, quantity, price, price_type="00", original_order_no=""):
        """
        - price_type – 00:지정가, 03:시장가
        - original_order_no – 원주문번호
        """
        self.dynamicCall("SendOrder(QString, QString, QString, int, QString, int, int, QString, QString)",
                          [rqname, screen_no, account_no, order_type, zongmok_code, quantity, price, price_type, original_order_no])
