import pandas as pd
from subprocess import Popen, PIPE
import os

df_kospi = pd.read_csv("market_value/kospi_naver.csv")
df_kosdaq = pd.read_csv("market_value/kosdaq_naver.csv")
df = pd.concat([df_kospi, df_kosdaq], ignore_index=True)
list_codes = list(df["code"])
for code in list_codes:
    code = code.replace(".KS", "")
    code = code.replace(".KQ", "")
    pipes = Popen("mv all_stock_trading_history/{}* all_stock_trading_history_non_ETF".format(code), stdout=PIPE, stderr=PIPE, shell=True)
    std_out, std_err = pipes.communicate()
