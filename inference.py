import os 
import time
import re
import sys
import argparse
from subprocess import Popen, PIPE
import datetime

import numpy as np

import models
import data_organizer
import data_extractor


from keras.models import model_from_json
import pandas as pd

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=int,
    help="GPU index",
    required=False,
    default=0
    )
FLAGS, _ = parser.parse_known_args()

ZONGMOK_SUGGESTION_DIR = "C:\\Users\\woals\\stock_prediction\\zongmok_suggestion"
GPU_INDEX = FLAGS.gpu_index
LIST_PERIOD_LEN_9to10 = [6, 10, 20, 30]
LIST_INPUT_LEN_9to10 = [3, 5, 10, 15]
LIST_PERIOD_LEN_after10 = [120, 180]
LIST_INPUT_LEN_after10 = [60, 60]
MODEL_DIR = "network_models"
DIR_HISTORY_FOR_INFERENCE = "history_for_inference"
HOME_PATH = os.path.dirname(os.path.realpath(__file__))
CRAWL_INTERVAL_MINUTE = 1

os.environ['CUDA_VISIBLE_DEVICES'] = str(GPU_INDEX)


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def load_network(dir_name, trainable=False):
    network_file = all_files_under(dir_name, extension=".json")
    weight_file = all_files_under(dir_name, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def zongmok_codes(home_path):
    kospi_code = pd.read_csv(os.path.join(home_path, "market_value", "kospi_naver.csv"))["code"].tolist()
    kosdaq_code = pd.read_csv(os.path.join(home_path, "market_value", "kosdaq_naver.csv"))["code"].tolist()
    list_codes = kospi_code + kosdaq_code
    list_codes = [code[:-3] for code in list_codes]  # strip .KS or .KQ
    return list_codes
    

def load_networks(list_period, list_input_len):
    list_networks = []
    for model_index in range(len(list_input_len)):
        model_path = os.path.join(MODEL_DIR, "{}_{}".format(list_period[model_index], list_input_len[model_index]))
        network = load_network(model_path)
        list_networks.append(network)
    return list_networks

# update koraeryang if korae_all files are outdated
koraeall_files = all_files_under("korae_all")
if len(koraeall_files) > 0:
    modified_date = np.max([int(str(datetime.datetime.fromtimestamp(os.path.getmtime(koraeall_file))).split(" ")[0].replace("-", "")) for koraeall_file in koraeall_files])
    today = int(str(datetime.datetime.today()).split(" ")[0].replace("-", ""))
    if datetime.datetime.today().weekday() <= 4 and modified_date < today:
        data_extractor.korae_all()        
else:
    data_extractor.korae_all()
    
# networks between 9 to 10 (before 10)
curr_hour = int(time.strftime("%H"))
if curr_hour < 10:
    list_networks = load_networks(LIST_PERIOD_LEN_9to10, LIST_INPUT_LEN_9to10)
    LIST_INPUT_LEN = LIST_INPUT_LEN_9to10
    isbetween9to10 = True
else:
    list_networks = load_networks(LIST_PERIOD_LEN_after10, LIST_INPUT_LEN_after10)
    LIST_INPUT_LEN = LIST_INPUT_LEN_after10
    isbetween9to10 = False

# wait until 09:00
prev_time = int(time.strftime("%H%M"))
while True:
    if prev_time > 900:
        break
    else:
        time.sleep(1)
        prev_time = int(time.strftime("%H%M"))

# load & normalize data
dtime = time.strftime("%Y%m%d") + "1600"
prev_time = int(time.strftime("%H%M"))
zongmok_suggestion_path = os.path.join(ZONGMOK_SUGGESTION_DIR, "{}.txt".format(datetime.datetime.now().strftime('%Y%m%d')))
while prev_time < 1515:
    zongmok_to_suggest = {}
    curr_hour = int(time.strftime("%H"))
    for code in zongmok_codes(HOME_PATH):
        fn = "{}_{}.csv".format(code, dtime)
        fpath = os.path.join(HOME_PATH, DIR_HISTORY_FOR_INFERENCE, fn)
        df = data_organizer.load_csv_inference(fpath)
        if df is None or len(df) < 2:  # no data
            continue
        # ignore mean koraerayng*price < 1500 man
        mat = df.values
        if np.mean(mat[:, 0] * mat[:, 5]) < 1.5e7:
            continue

        # run inference
        period = [mat]
        misc_data = data_organizer.misc_data(code=code, date=int(dtime), data_type=["korae_all"])
        normalized_period = data_organizer.normalize(period, misc_data)
        if normalized_period is None:
            continue
        list_pred_mean, list_pred_std = [], []
        for model_index in range(len(LIST_INPUT_LEN)):
            arr_normalized_period = np.array(normalized_period)
            truncated_normalized_period = arr_normalized_period[:, -LIST_INPUT_LEN[model_index]:, :]
            if len(truncated_normalized_period[0]) == LIST_INPUT_LEN[model_index]:
                if len(np.unique(truncated_normalized_period[0][:, 0])) == 1:  # hard penalty to non-changing zongmok
                    mean, std = 0, 0
                else:
                    val_pred = list_networks[model_index].predict(truncated_normalized_period, batch_size=1)
                    val_pred_float = val_pred[0, 0]
                    mean, std = val_pred_float, np.sqrt(val_pred_float * (1 - val_pred_float))
                list_pred_mean.append(mean)
                list_pred_std.append(std)

        # save if predicted to soar
        if len(list_pred_mean) > 0 and ((isbetween9to10 and np.mean(list_pred_mean) > 0.1) or
                                        (not isbetween9to10 and np.mean(list_pred_mean) > 0.8)):
           zongmok_to_suggest[code] = list_pred_mean

    # write to a file
    while True:
        try:
            with open(zongmok_suggestion_path, "a") as f:
                f.write("**{}**\n".format(time.strftime("%H:%M")))
                for zongmok_code, list_pred_mean in zongmok_to_suggest.items():
                    f.write("{} - {} | ".format(zongmok_code, list_pred_mean))
                f.write("\n")
            break
        except:
            time.sleep(0.1)
        
            
    # flip model if 9to10 models were being used and it passed 10
    if curr_hour >= 10 and isbetween9to10:
        list_networks = load_networks(LIST_PERIOD_LEN_after10, LIST_INPUT_LEN_after10)
        LIST_INPUT_LEN = LIST_INPUT_LEN_after10
        isbetween9to10 = False

    # sleep until CRAWL_INTERVAL_MINUTE minutes pass
    curr_time = int(time.strftime("%H%M"))
    while curr_time - prev_time < CRAWL_INTERVAL_MINUTE:
        time.sleep(1)
        curr_time = int(time.strftime("%H%M"))
    prev_time = curr_time
    
