import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
import data_organizer
import re

PERIOD_LEN = 120
INPUT_LEN = 60
GAIN = 0.05
TOLERANCE = -0.01


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


# load data
# fpaths = all_files_under("all_stock_trading_history")
# fpaths = all_files_under("soaring_stock_trading_history")[:10]
# non_soaring_periods, soaring_periods = [], []
# for fpath in fpaths:
#     df = data_organizer.load_csv(fpath)
#     code, tail = os.path.basename(fpath).split("_") 
#     date = re.sub(".csv$", "", tail)
#     misc_data = data_organizer.misc_data(code=code, date=int(date), data_type=["korae_all"])
#     list_non_soaring_periods, list_soaring_periods = data_organizer.split_periods(df, period_len=PERIOD_LEN, input_len=INPUT_LEN, tolerance=TOLERANCE, gain=GAIN)
#     non_soaring_periods += list_non_soaring_periods
#     soaring_periods += list_soaring_periods
# 
# inputs, labels = data_organizer.beginning_sequence(non_soaring_periods, soaring_periods, PERIOD_LEN, INPUT_LEN)
# 
# for index in range(len(inputs)):
#     seq=inputs[index]
#     if labels[index]==1:
#         plt.plot([1]*len(seq), seq[:,0]*seq[:,5], color="r")
#     else:
#         plt.plot([0]*len(seq), seq[:,0]*seq[:,5], color="b")
#         
# plt.xlabel("label")
# plt.ylabel("total price")
# plt.savefig("koraerayng_seperation.png")
    
# fpath="history_for_inference/009320_201901181600.csv"
# df = data_organizer.load_csv(fpath)
# mat=df.as_matrix(columns=df.columns)
# plt.scatter(range(len(mat)), mat[:,0]*mat[:,5], s=5) 
# plt.xlabel("time")
# plt.ylabel("total price")
# plt.savefig("koraerayng_{}.png".format(os.path.basename(fpath).split("_")[0]))
    
    