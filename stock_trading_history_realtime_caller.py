import crawler
import sys
import time
import os
import pandas as pd
import multiprocessing

HOME_PATH = os.path.dirname(os.path.realpath(__file__))
CRAWL_INTERVAL_MINUTE = 1
DIR_HISTORY_FOR_INFERENCE = "history_for_inference"
N_PROCESSES = 2


def zongmok_codes(home_path):
    kospi_code = pd.read_csv(os.path.join(home_path, "market_value", "kospi_naver.csv"))["code"].tolist()
    kosdaq_code = pd.read_csv(os.path.join(home_path, "market_value", "kosdaq_naver.csv"))["code"].tolist()
    list_codes = kospi_code + kosdaq_code
    list_codes = [code[:-3] for code in list_codes]  # strip .KS or .KQ
    return list_codes


def crawl_history_realtime(args):
    zongmok_codes, dtime, path_history_for_inference = args

    # wait until 09:00
    prev_time = int(time.strftime("%H%M"))
    while True:
        if prev_time > 900:
            break
        else:
            time.sleep(1)
            prev_time = int(time.strftime("%H%M"))

    # loop until 09:00
    prev_time = int(time.strftime("%H%M"))
    while True:
        if prev_time > 1515:
            break
        st = time.time()
        for code in zongmok_codes:
            crawler.stock_trading_history_realtime(code, dtime, path_history_for_inference)
        # sleep until CRAWL_INTERVAL_MINUTE minutes passe
        curr_time = int(time.strftime("%H%M"))
        while curr_time - prev_time < CRAWL_INTERVAL_MINUTE:
            time.sleep(1)
            curr_time = int(time.strftime("%H%M"))
        prev_time = curr_time

    
if __name__ == '__main__': # main guard is required for window
    list_zongmok_codes = zongmok_codes(HOME_PATH)
    # divide zongmok codes 
    zongmok_codes = [[] for __ in range(N_PROCESSES)]
    chunk_sizes = len(list_zongmok_codes) // N_PROCESSES
    for index in range(N_PROCESSES):
        if index == N_PROCESSES - 1:  # allocate ranges (last GPU takes remainders)
            start, end = index * chunk_sizes, len(list_zongmok_codes)
        else:
            start, end = index * chunk_sizes, (index + 1) * chunk_sizes
        zongmok_codes[index] = list_zongmok_codes[start:end]

    # run multiple processes
    date = time.strftime("%Y%m%d")
    dtime = date + "1600"
    path_history_for_inference = os.path.join(HOME_PATH, DIR_HISTORY_FOR_INFERENCE)
    pool = multiprocessing.Pool(processes=N_PROCESSES)
    pool.map(crawl_history_realtime, [(zongmok_codes[i], dtime, path_history_for_inference) for i in range(N_PROCESSES)])
