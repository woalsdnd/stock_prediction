import os 
import re
from sklearn.metrics.ranking import roc_auc_score
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils import class_weight
import sys
import argparse
from subprocess import Popen, PIPE

import models
import data_organizer
import time
from keras.models import model_from_json
import pandas as pd
import code

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=int,
    required=True
    )
parser.add_argument(
    '--dir_history',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

GPU_INDEX = FLAGS.gpu_index
LIST_PERIOD_LEN = [6, 10, 20, 30]
LIST_INPUT_LEN = [3, 5, 10, 15]
MODEL_DIR = "network_models"
HOME_PATH = os.path.dirname(os.path.realpath(__file__))
GAIN = 0.05
TOLERANCE = -0.01

os.environ['CUDA_VISIBLE_DEVICES'] = str(GPU_INDEX)


def binary_stats(y_true, y_pred):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    AUC_ROC = roc_auc_score(y_true, y_pred)
    list_thresh, list_prec, list_acc, list_sen = [], [], [], []
    for percentile in np.linspace(98,100,50):
        y_pred_copy = np.copy(y_pred)
        threshold = np.percentile(y_pred, percentile)
        y_pred_copy[y_pred_copy >= threshold] = 1
        y_pred_copy[y_pred_copy < threshold] = 0
        cm = confusion_matrix(y_true, y_pred_copy)
        prec = 1.*cm[1, 1] / (cm[0, 1] + cm[1, 1]) if cm[0, 1] + cm[1, 1] != 0 else 0 
        list_thresh.append(threshold)
        list_prec.append(prec)
        list_acc.append(1.*np.trace(cm) / np.sum(cm))
        list_sen.append(1.*cm[1, 1] / (cm[1, 0] + cm[1, 1]))
    return list_thresh, list_prec, list_acc, list_sen, AUC_ROC


# convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=1):
    dataX, dataY = [], []
    for i in range(len(dataset) - look_back - 1):
        a = dataset[i:(i + look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
    return numpy.array(dataX), numpy.array(dataY)


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def load_network(dir_name, trainable=False):
    network_file = all_files_under(dir_name, extension=".json")
    weight_file = all_files_under(dir_name, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def zongmok_codes(home_path):
    kospi_code = pd.read_csv(os.path.join(home_path, "market_value", "kospi_naver.csv"))["code"].tolist()
    kosdaq_code = pd.read_csv(os.path.join(home_path, "market_value", "kosdaq_naver.csv"))["code"].tolist()
    list_codes = kospi_code + kosdaq_code
    list_codes = [code[:-3] for code in list_codes]  # strip .KS or .KQ
    return list_codes
    

# load networks    
list_networks = []
for model_index in range(len(LIST_INPUT_LEN)):
    model_path = os.path.join(MODEL_DIR, "{}_{}".format(LIST_PERIOD_LEN[model_index], LIST_INPUT_LEN[model_index]))
    network = load_network(model_path)
    list_networks.append(network)

# run back test
fpaths = all_files_under(FLAGS.dir_history)
list_preds, list_true = {input_len:[] for input_len in LIST_INPUT_LEN}, {input_len:[] for input_len in LIST_INPUT_LEN}
for fpath in fpaths:
    code, tail = os.path.basename(fpath).split("_") 
    dtime = re.sub(".csv$", "", tail)
    fpath = os.path.join(HOME_PATH, FLAGS.dir_history, os.path.basename(fpath))
    df = data_organizer.load_csv_inference(fpath)
    if df is None or len(df) < 2:
        continue
    if np.max(LIST_INPUT_LEN) <= 15:
        df = df.iloc[:60]
    misc_data = data_organizer.misc_data(code=code, date=int(dtime), data_type=["korae_all"])
    for model_index in range(len(LIST_INPUT_LEN)):
        list_non_soaring_periods, list_soaring_periods = data_organizer.split_periods(df, period_len=LIST_PERIOD_LEN[model_index], input_len=LIST_INPUT_LEN[model_index], tolerance=TOLERANCE, gain=GAIN) 
        non_soaring_periods = data_organizer.normalize(list_non_soaring_periods, misc_data)
        soaring_periods = data_organizer.normalize(list_soaring_periods, misc_data)
        if non_soaring_periods is None or soaring_periods is None:
            continue
        inputs, labels = data_organizer.beginning_sequence(non_soaring_periods, soaring_periods, LIST_INPUT_LEN[model_index])
        list_true[LIST_INPUT_LEN[model_index]] += list(labels)
#         # plot sample sequence
#         plt.close()
#         for index_soaring in np.random.choice(range(len(soaring_periods)), 10):
#             plt.plot(range(len(soaring_periods[index_soaring][:, 0])), soaring_periods[index_soaring][:, 0], color="r")
#         for index_non_soaring in np.random.choice(range(len(non_soaring_periods)), 10):
#             plt.plot(range(len(non_soaring_periods[index_non_soaring][:, 0])), non_soaring_periods[index_non_soaring][ :, 0], color="b")
#         plt.xlabel("minutes")
#         plt.ylabel("price")
#         plt.savefig(os.path.join("input_checks", "backtest_periods_sample_{}_{}.png".format(LIST_PERIOD_LEN[model_index], LIST_INPUT_LEN[model_index])))
        for network_input in inputs:
            val_pred = list_networks[model_index].predict(np.expand_dims(network_input, axis=0), batch_size=1)
            list_preds[LIST_INPUT_LEN[model_index]].append(val_pred[0, 0])
        
# measure performance
for input_len in list_preds.keys():
    arr_true = np.array(list_true[input_len])
    arr_pred = np.array(list_preds[input_len])
    if len(arr_true[arr_true == 1]) > 0:
        list_threshold, list_val_prec, list_val_acc, list_val_sen, val_AUC_ROC = binary_stats(arr_true, arr_pred)
        print("INPUT LEN {} => AUROC: {}".format(input_len, val_AUC_ROC))
        for index in range(len(list_val_prec)):
            print("thresh: {}, prec: {}, acc: {}, sen: {}".format(list_threshold[index], list_val_prec[index], list_val_acc[index], list_val_sen[index]))
        
