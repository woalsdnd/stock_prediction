import sys
import time
import os
from datetime import datetime
from functools import wraps

import numpy as np

from PyQt5.QtWidgets import *
import Kiwoom

MAX_BUDGET = 1000000
MAX_N_ZONGMOK = 10
MAX_BUDGET_PER_ZONGMOK = MAX_BUDGET // MAX_N_ZONGMOK
TR_REQ_TIME_INTERVAL = 1
NO_ACCOUNT = "8117805611"
PASSWORD = "0000"
STALE_MAESOO_SEC = 60
LOSS_TOLERANCE = 0.02
PROFIT_MARGIN = 0.005
SELL_MARGINOT_ALPHA = 0.85 # a*higest_maesoo_price + (1-a)*breakeven
ZONGMOK_SUGGESTION_DIR = "C:\\Users\\woals\\stock_prediction\\zongmok_suggestion"

def commRQdata(func):
    @wraps(func)
    def sleep(self, *args, **kwargs):
        time.sleep(TR_REQ_TIME_INTERVAL)
        query_result = func(self, *args, **kwargs)
        return query_result

    return sleep

class kiwoom_trader:

    def __init__(self):
        self.kiwoom = Kiwoom.Kiwoom()
        self.kiwoom.comm_connect()
        self.is_receiving_real_data = False

    @commRQdata
    def get_hoga(self, zongmok_code):
        self.kiwoom.set_input_value("종목코드",zongmok_code)
        self.kiwoom.comm_rq_data("현재호가","opt10004", 0, "5001")
        while not "list_maedo" in self.kiwoom.buffer:
            time.sleep(TR_REQ_TIME_INTERVAL)
            self.kiwoom.comm_rq_data("현재호가","opt10004", 0, "5001")
        return self.kiwoom.buffer

    @commRQdata
    def buy(self, zongmok_code, price, quantity):
        self.kiwoom.send_order("주식주문", "5002", NO_ACCOUNT, 1, zongmok_code, quantity, price,
                               price_type="00",original_order_no="")

    @commRQdata
    def sell(self, zongmok_code, quantity):
        self.kiwoom.send_order("주식주문", "5002", NO_ACCOUNT, 2, zongmok_code, quantity, 0,
                               price_type="03", original_order_no="")

    @commRQdata
    def cancel_buy(self, zongmok_code, quantity, original_order_no):
        self.kiwoom.send_order("주식주문", "5002", NO_ACCOUNT, 3, zongmok_code, quantity, 0,
                               price_type="00", original_order_no=original_order_no)

    @commRQdata
    def jango(self):
        self.kiwoom.set_input_value("계좌번호", NO_ACCOUNT)
        self.kiwoom.set_input_value("비밀번호", PASSWORD)
        self.kiwoom.set_input_value("상장폐지조회구분", "1")
        self.kiwoom.set_input_value("비밀번호입력매체구분", "00")
        self.kiwoom.comm_rq_data("계좌평가잔고내역요청", "opw00004", 0, "5003")
        while type(self.kiwoom.buffer) != str:
            time.sleep(TR_REQ_TIME_INTERVAL)
            self.kiwoom.comm_rq_data("계좌평가잔고내역요청", "opw00004", 0, "5003")
        return self.kiwoom.buffer

    @commRQdata
    def michaegyol(self, maemae_type):
        self.kiwoom.set_input_value("계좌번호", NO_ACCOUNT)
        self.kiwoom.set_input_value("비밀번호", PASSWORD)
        self.kiwoom.set_input_value("체결구분", "1")
        if maemae_type == "maedo":
            maemae_type_code = "1"
        elif maemae_type == "maesoo":
            maemae_type_code = "2"
        else:
            raise ValueError("maemae_type should be either maedo or maesoo")
        self.kiwoom.set_input_value("매매구분", maemae_type_code)
        self.kiwoom.comm_rq_data("실시간미체결요청", "opt10075", 0, "5004")
        while not "list_michaegyol" in self.kiwoom.buffer:
            time.sleep(TR_REQ_TIME_INTERVAL)
            self.kiwoom.comm_rq_data("실시간미체결요청", "opt10075", 0, "5004")
        return self.kiwoom.buffer

    @commRQdata
    def holding_zongmok(self):
        self.kiwoom.set_input_value("계좌번호", NO_ACCOUNT)
        self.kiwoom.comm_rq_data("계좌수익률요청", "opt10085", 0, "5005")
        while not "list_maeip" in self.kiwoom.buffer:
            time.sleep(TR_REQ_TIME_INTERVAL)
            self.kiwoom.comm_rq_data("계좌수익률요청", "opt10085", 0, "5005")
        return self.kiwoom.buffer

    def set_zongmok_for_monitoring(self, list_zongmok):
        zongmok_codes = ";".join(list_zongmok)
        fids = ";".join([str(i) for i in range(40,80)])
        if not self.is_receiving_real_data:
            self.kiwoom.set_real_reg("6002", zongmok_codes, fids, "0")
            self.is_receiving_real_data = True
            print("real data registered a new")
        else:
            self.kiwoom.set_real_reg("6002", zongmok_codes, fids, "1")
            print("real data registered additionally")
        
    def remove_zongmok_from_monitoring(self, zongmok_code):
        self.kiwoom.set_real_reg("6002", zongmok_code)
    


def isbuy_v0(hoga_info):
    """
    Judge whether to buy or not from hoga janryang.
    Buy if maesooryang / maedoryang up to 3 and 5 chasun are both above 2.

    Args:
        hoga_info (dict): hoga infomration
                            key => list_maedo, list_maesoo, list_maedo_janryang, list_maesoo_janryang
                            value => str (list_maedo: "+price", list_maesoo: "-price", list_*_janryang: "volume")

    Returns:
         bool for buying signal
    """
    demand_supply_ratio = 2
    arr_maedo=np.array([int(str_price.replace("+","")) for str_price in hoga_info["list_maedo"]])
    arr_maesoo = np.array([int(str_price.replace("-", "")) for str_price in hoga_info["list_maesoo"]])
    arr_maedo_janryang = np.array([int(str_volume) for str_volume in hoga_info["list_maedo_janryang"]])
    arr_maesoo_janryang = np.array([int(str_volume) for str_volume in hoga_info["list_maesoo_janryang"]])
    maedo_janryang_3chasun = np.sum(arr_maedo_janryang[:3])
    maesoo_janryang_3chasun = np.sum(arr_maesoo_janryang[:3])
    maedo_janryang_5chasun = np.sum(arr_maedo_janryang[:5])
    maesoo_janryang_5chasun = np.sum(arr_maesoo_janryang[:5])
    ratio_3chasun = 1. * maesoo_janryang_3chasun / (maedo_janryang_3chasun + 0.1)
    ratio_5chasun = 1. * maesoo_janryang_5chasun / (maedo_janryang_5chasun + 0.1)
    return ratio_3chasun > demand_supply_ratio and ratio_5chasun > demand_supply_ratio and arr_maedo[0] != 0

def get_zongmok_code(lines):
    """
    Args:
        lines (list of str): list of two last lines in the suggestion file

    Returns:
        list_zongmok (list): list of suggested zongmok codes. None if no zongmok exists
    """
    if not "**" in lines[0]:
        return None
    else:
        list_zongmok = []
        list_code_prob = lines[1].split("|")
        for code_prob in list_code_prob:
            code = code_prob.split(" - ")[0].strip()
            if len(code) > 0:
                list_zongmok.append(code)
        return list_zongmok

def read_zongmok_suggestion_file(zongmok_suggestion_path):
    while True:
        try:
            with open(zongmok_suggestion_path, "r") as f:
                lines = f.read().splitlines()
                return get_zongmok_code(lines[-2:])
        except:
            time.sleep(1)

if __name__ == "__main__":
    # initialize API
    app = QApplication(sys.argv)
    trader=kiwoom_trader()

    zongmok_suggestion_path = os.path.join(ZONGMOK_SUGGESTION_DIR, "{}.txt".format(datetime.now().strftime('%Y%m%d')))
    zongmok_suggestion_path = os.path.join(ZONGMOK_SUGGESTION_DIR, "20190503.txt")
    holding_zongmok_info = {}

    # wait until 09:00
    prev_time = int(time.strftime("%H%M"))
    while True:
        if prev_time > 900:
            break
        else:
            time.sleep(1)
            prev_time = int(time.strftime("%H%M"))

    # sell all
    #dict_holding_zongmok = trader.holding_zongmok()
    #list_zongmok_code, list_quantity = dict_holding_zongmok["list_zongmok_code"], dict_holding_zongmok["list_quantity"]
    #for index in range(len(list_zongmok_code)):
    #    zongmok_code, quantity = list_zongmok_code[index], int(list_quantity[index])
    #    if quantity != 0:
    #        trader.sell(zongmok_code, quantity)

    #loop until 1515
    curr_time = time.strftime("%H%M")
    while int(curr_time) < 1515:
        curr_time = time.strftime("%H%M")

        # read zongmok code suggested from the predictor
        print("read zongmok code")
        list_zongmok_codes = read_zongmok_suggestion_file(zongmok_suggestion_path)
        zongmok_suggestion_modified_time = str(datetime.fromtimestamp(os.path.getmtime(zongmok_suggestion_path))).split()[1]

        # judge whether to buy from hoga information
        print("judge zongmok")
        for zongmok_code in list_zongmok_codes:
            trader.set_zongmok_for_monitoring(list_zongmok_code)
            while not zongmok_code in trader.kiwoom.hoga_info:
                time.sleep(1)    
            hoga_info = trader.kiwoom.hoga_info(zongmok_code)
            buying_signal = isbuy_v0(hoga_info)
            buying_signal=True
            if buying_signal:
                # buy
                maedo_choiwoosun = int(hoga_info["list_maedo"][0].replace("+",""))
                maesoo_choiwoosun = int(hoga_info["list_maedo"][0].replace("-", ""))
                jango = int(trader.jango() if trader.kiwoom.jango is None else int(trader.kiwoom.jango)
                quantity = min(MAX_BUDGET_PER_ZONGMOK, jango) // maedo_choiwoosun
                print("zongmok:{}, jango: {}, price: {}, quantity: {}".format(zongmok_code, jango,maedo_choiwoosun, quantity)) 
                if (quantity > 0) and (zongmok_code not in holding_zongmok_info.keys()):
                    trader.buy(zongmok_code, maedo_choiwoosun, quantity)
                    holding_zongmok_info[zongmok_code]={"buy_order_time": time.time(),
                                                        "lower_bound":int(maedo_choiwoosun * (1 - LOSS_TOLERANCE)),
                                                        "breakeven":int(maedo_choiwoosun * (1 + PROFIT_MARGIN)),
                                                        "higest_maesoo_choiwoosun": maesoo_choiwoosun}
            else:
                trader.remove_zongmok_from_monitoring(zongmok_code)                            
            
        # delete stale michaegyol maesoo in 2 cases
        # 1. no info exists in dictionary => it means trader.py halted once
        # 2. could not buy for long period of time (STALE_MAESOO_SEC)
        print("clean up michaegyol")
        dict_michaegyol = trader.kiwoom.michaegyol_zongmok
        list_zongmok_code, list_michaegyol, list_org_order_no = dict_michaegyol["list_zongmok_code"], dict_michaegyol["list_michaegyol"], dict_michaegyol["list_org_order_no"] 
        for index in range(len(list_zongmok_code)):
            zongmok_code, michaegyol, org_order_no = list_zongmok_code[index], list_michaegyol[index], list_org_order_no[index]
            if (zongmok_code not in holding_zongmok_info) or (time.time() - holding_zongmok_info[zongmok_code]["buy_order_time"] > STALE_MAESOO_SEC):
                trader.cancel_buy(zongmok_code, michaegyol, org_order_no)

        # monitor price of holding zongmok until new suggestion comes out    
        while zongmok_suggestion_modified_time == str(datetime.fromtimestamp(os.path.getmtime(zongmok_suggestion_path))).split()[1]:
            # sell at market price in 2 cases
            #  1. maesoo_choiwoosun <= lower_bound
            #  2. maesoo_choiwoosun <= sell_marginot and maesoo_choiwoosun >= breakeven
            dict_holding_zongmok = trader.kiwoom.holding_zongmok
            list_zongmok_code, list_quantity, list_maeip = dict_holding_zongmok["list_zongmok_code"], dict_holding_zongmok["list_quantity"], dict_holding_zongmok["list_maeip"]
            for index in range(len(list_zongmok_code)):
                zongmok_code, quantity, maeip = list_zongmok_code[index], int(list_quantity[index]), int(list_maeip[index])
                if quantity != 0:
                    hoga_info = trader.kiwoom.hoga_info(zongmok_code)
                    curr_maesoo_choiwoosun = int(hoga_info["list_maesoo"][0].replace("-", ""))
                    if zongmok_code not in holding_zongmok_info:
                        holding_zongmok_info[zongmok_code]={"buy_order_time": time.time(),
                                                            "lower_bound":int(maeip * (1 - LOSS_TOLERANCE)),
                                                            "breakeven":int(maeip * (1 + PROFIT_MARGIN)),
                                                            "higest_maesoo_choiwoosun": curr_maesoo_choiwoosun}
                    highest_maesoo_choiwoosun = holding_zongmok_info[zongmok_code]["higest_maesoo_choiwoosun"]
                    breakeven = holding_zongmok_info[zongmok_code]["breakeven"]
                    lower_bound = holding_zongmok_info[zongmok_code]["lower_bound"]
                    print(holding_zongmok_info[zongmok_code])
                    if curr_maesoo_choiwoosun >= highest_maesoo_choiwoosun: # rising
                        holding_zongmok_info[zongmok_code]["higest_maesoo_choiwoosun"] = curr_maesoo_choiwoosun
                    else: # diminishing
                        if lower_bound >= curr_maesoo_choiwoosun:
                            trader.sell(zongmok_code, quantity)
                            trader.remove_zongmok_from_monitoring(zongmok_code)
                            holding_zongmok_info.pop(zongmok_code)
                        else:
                            sell_marginot = int(SELL_MARGINOT_ALPHA * highest_maesoo_choiwoosun + (1-SELL_MARGINOT_ALPHA) * breakeven)
                            if curr_maesoo_choiwoosun <= sell_marginot and curr_maesoo_choiwoosun >= breakeven:
                                trader.sell(zongmok_code, quantity)
                                trader.remove_zongmok_from_monitoring(zongmok_code)
                                holding_zongmok_info.pop(zongmok_code)

# sell all
dict_holding_zongmok = trader.kiwoom.holding_zongmok
list_zongmok_code, list_quantity = dict_holding_zongmok["list_zongmok_code"], dict_holding_zongmok["list_quantity"]
for index in range(len(list_zongmok_code)):
    zongmok_code, quantity = list_zongmok_code[index], int(list_quantity[index])
    if quantity != 0:
        trader.sell(zongmok_code, quantity)
