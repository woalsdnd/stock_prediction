import os
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from bs4 import BeautifulSoup
import pandas as pd
import glob
import numpy as np

N_PROPERTIES_AT_A_TIME = 6
N_TOTAL_PROPERTIES = 27
N_DEFAULT_PROPERTIES = 6

def get_checkboxes(driver):
    # get property buttons
    dict_checkboxes = {}
    for index in range(1, N_TOTAL_PROPERTIES+1):
        button = driver.find_element_by_id('option{}'.format(index))
        dict_checkboxes[index] = button
    return dict_checkboxes


def convert_to_float(str):
    if str == 'N/A':
        return float('-inf')
    else:
        return float(str.replace(",", ""))

def market_value(path_outdir):
    """
    crawl market values from naver.com 
    
    :path_outdir: str for the path to output directory
    """
    
    driver = webdriver.Chrome('./chromedriver')
    # get total pages
    n_pages = {}
    for index_type, stype in enumerate(["kospi", "kosdaq"]):
        driver.get('http://finance.naver.com/sise/sise_market_sum.nhn?sosok={}'.format(index_type))
        page_source = driver.page_source
        soup = BeautifulSoup(page_source, "html.parser")
        tds = soup.findAll('td', {'class' : 'pgRR'})
        final_page_str = tds[0].find('a')['href'][-10:]
        n_pages[stype] = int(final_page_str[final_page_str.find("page=") + 5:])
    print(n_pages)
    
    # get properties
    dict_properties = {}
    for index in range(1, N_TOTAL_PROPERTIES+1):
        button = driver.find_element_by_id('option{}'.format(index))
        dict_properties[index] = button.get_attribute("value")
    # turn off default properties
    dict_checkboxes = get_checkboxes(driver)
    for default_index in [1, 4, 6, 12, 15, 21]:
        dict_checkboxes[default_index].click()

    for crawl_index in range(int(np.ceil(1.*N_TOTAL_PROPERTIES/N_PROPERTIES_AT_A_TIME))):
        indices_properties = range(crawl_index*N_PROPERTIES_AT_A_TIME+1, min((crawl_index+1)*N_PROPERTIES_AT_A_TIME+1, N_TOTAL_PROPERTIES+1))
        selected_properties = [dict_properties[index_property] for index_property in indices_properties]

        # select properties
        dict_checkboxes = get_checkboxes(driver)
        for index_property in indices_properties:
            dict_checkboxes[index_property].click()
        # click submit button
        submit_button = driver.find_element_by_xpath("//div[@class='item_btn']/a[1]")
        submit_button.click()

        for index_type, stype in enumerate(["kospi", "kosdaq"]):
            stock_code_list = list()
            stock_name_list = list()
            stock_price_list = list()
            stock_return_list = list()
            dict_selected_properties = {prop:[] for prop in selected_properties} # naver 증권 페이지에서 선택된 항목
            
            # get the last page
            for i in range(1, n_pages[stype] + 1):
                driver.get('http://finance.naver.com/sise/sise_market_sum.nhn?sosok={}&page={}'.format(index_type, i))

                # gen dict for matching korean and english
                dict_option_korean_english = {}
                for index in range(1, len(indices_properties)+1):
                    property_korean = driver.find_element_by_xpath("//td[@class='choice'][{}]/label".format(index)).get_attribute("innerHTML")
                    dict_option_korean_english[property_korean] = driver.find_element_by_xpath("//td[@class='choice'][{}]/input".format(index)).get_attribute("value")
                selected_properties_korean = {}
                for index_property in range(1, len(indices_properties)+1):
                    property_korean = driver.find_element_by_xpath("//table[@class='type_2']/thead/tr/th[{}]".format(N_DEFAULT_PROPERTIES+index_property)).get_attribute("innerHTML") # skip default elements (no, code, name, etc)
                    selected_properties_korean[index_property] = property_korean

                page_source = driver.page_source
                soup = BeautifulSoup(page_source, "html.parser")
                trs = soup.findAll('tr', {'onmouseover' : 'mouseOver(this)'})
                for tr in trs:
                    tds = tr.findAll('td')
                    for index, td in enumerate(tds):
                        # store info into lists
                        if index == 1:
                            stock_code_list.append(td.find('a')['href'][-6:] + ".KS" if stype == "kospi" else td.find('a')['href'][-6:] + ".KQ")
                            stock_name_list.append(td.find('a').contents[0])
                        elif index == 2:
                            stock_price_list.append(int(td.contents[0].replace(",", "")))
                        elif index == 4:
                            stock_return_list.append(float(td.find('span').contents[0].replace("%", "")))
                        elif index >= N_DEFAULT_PROPERTIES and index < N_DEFAULT_PROPERTIES+len(selected_properties):
                            option_korean = selected_properties_korean[index+1-N_DEFAULT_PROPERTIES]
                            option = dict_option_korean_english[option_korean]
                            dict_selected_properties[option].append(convert_to_float(td.contents[0]))
            dict_selected_properties.update({"code":stock_code_list, "name":stock_name_list, "price":stock_price_list, "return":stock_return_list})
            df = pd.DataFrame(dict_selected_properties)
            file_home_path = os.path.dirname(os.path.realpath(__file__))
            df.to_csv(os.path.join(file_home_path, path_outdir, stype + "_naver_{}.csv".format("_".join(selected_properties))), index=False)

        # unselect properties
        dict_checkboxes = get_checkboxes(driver)
        for index_property in indices_properties:
            dict_checkboxes[index_property].click()


path_outdir="crawled_data"
file_home_path = os.path.dirname(os.path.realpath(__file__))
# market_value(path_outdir)

list_market = ["kospi", "kosdaq"]
for market in list_market:
    list_csv_paths = glob.glob(os.path.join(file_home_path, path_outdir)+"/{}_*.csv".format(market))
    list_df = []
    for csv_path in list_csv_paths:
        list_df.append(pd.read_csv(csv_path))
    df_merged = list_df[0]
    for index in range(1, len(list_df)):
        df_next = list_df[index]
        df_next.drop(["code","price","return"], axis=1, inplace=True)
        df_merged = pd.merge(df_merged, df_next, on="name")
    df_merged.to_csv(os.path.join(file_home_path, "{}.csv".format(market)), index=False)