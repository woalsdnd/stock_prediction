import pandas as pd
from pandas_datareader import data
import fix_yahoo_finance as yf
import os
import time

# read stock lists
df_kospi=pd.read_csv("./kospi_naver.csv")
df_kosdaq=pd.read_csv("./kosdaq_naver.csv")

# filter with conditions
df_kospi_normal=df_kospi[(df_kospi["PER"]<10) & (df_kospi["PER"]>0) & (df_kospi["ROE"]>0)]
df_kosdaq_normal=df_kosdaq[(df_kosdaq["PER"]<10) & (df_kosdaq["PER"]>0) & (df_kosdaq["ROE"]>0)]

# print out
print df_kospi_normal.sort_values(by=["PER"])[["PER","ROE","name"]]
print df_kosdaq_normal.sort_values(by=["PER"])[["PER","ROE","name"]]
