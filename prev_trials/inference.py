import os 
import re
from sklearn.metrics.ranking import roc_auc_score
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils import class_weight
import sys
import argparse

import models
import data_organizer
import time
from keras.models import model_from_json
import pandas as pd

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=int,
    help="GPU index",
    required=True
    )
parser.add_argument(
    '--period_len',
    type=int,
    required=True
    )
parser.add_argument(
    '--input_len',
    type=int,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

GPU_INDEX = FLAGS.gpu_index
PERIOD_LEN = FLAGS.period_len
INPUT_LEN = FLAGS.input_len
MODEL_DIR = os.path.join("network_models", "{}_{}".format(PERIOD_LEN, INPUT_LEN))
DIR_HISTORY_FOR_INFERENCE = "history_for_inference"
HOME_PATH = os.path.dirname(os.path.realpath(__file__))

os.environ['CUDA_VISIBLE_DEVICES'] = str(GPU_INDEX)


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)


def binary_stats(y_true, y_pred):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
    prec = 1.*cm[1, 1] / (cm[0, 1] + cm[1, 1])
    acc = 1.*np.trace(cm) / np.sum(cm)
    sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
    AUC_ROC = roc_auc_score(y_true, y_pred)
    return prec, acc, sen, AUC_ROC


# convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=1):
    dataX, dataY = [], []
    for i in range(len(dataset) - look_back - 1):
        a = dataset[i:(i + look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back, 0])
    return numpy.array(dataX), numpy.array(dataY)


def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def load_network(dir_name, trainable=False):
    network_file = all_files_under(dir_name, extension=".json")
    weight_file = all_files_under(dir_name, extension=".h5")
    assert len(network_file) == 1 and len(weight_file) == 1
    with open(network_file[0], 'r') as f:
        network = model_from_json(f.read())
    network.load_weights(weight_file[0])
    network.trainable = trainable
    for l in network.layers:
        l.trainable = trainable
    return network


def zongmok_codes(home_path):
    kospi_code = pd.read_csv(os.path.join(home_path, "market_value", "kospi_naver.csv"))["code"].tolist()
    kosdaq_code = pd.read_csv(os.path.join(home_path, "market_value", "kosdaq_naver.csv"))["code"].tolist()
    list_codes = kospi_code + kosdaq_code
    list_codes = [code[:-3] for code in list_codes]  # strip .KS or .KQ
    return list_codes
    
    
# create and fit the LSTM network
network = load_network(MODEL_DIR)

# load & normalize data
for zongmok_code in zongmok_codes(HOME_PATH):
    date = time.strftime("%Y%m%d")
    fn = "{}_{}.csv".format(zongmok_code, date)
    
    fn = "000545_201811071600.csv"
    
    fpath = os.path.join(HOME_PATH, DIR_HISTORY_FOR_INFERENCE, fn)
    df = data_organizer.load_csv(fpath)
    code, tail = os.path.basename(fn).split("_") 
    date = re.sub(".csv$", "", tail)
    misc_data = data_organizer.misc_data(code=code, date=int(date), data_type=["korae_all"])
    
    list_period, list_normalized_period, list_mean = [], [], []
    for start in range(len(df) - INPUT_LEN):
        period = [df.iloc[start:start + INPUT_LEN].as_matrix(columns=df.columns)]
        normalized_period = np.array(data_organizer.normalize(period, misc_data))
        list_period.append(period)
        list_normalized_period.append(normalized_period)
        
        # make predictions
        if len(np.unique(normalized_period[0, :, 0])) == 1:  # exclude non-changing zongmok
            mean, std = 0, 0
        else:
            val_pred = network.predict(normalized_period, batch_size=1)
            val_pred_float = val_pred[0, 0]
            mean, std = val_pred_float, np.sqrt(val_pred_float * (1 - val_pred_float))
        list_mean.append(mean)
        print mean, std
        
    print list_period[np.argmax(list_mean)], list_normalized_period[np.argmax(list_mean)]
    break
