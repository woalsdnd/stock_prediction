from keras.models import Sequential, Model
from keras.layers import Input, Dense, LSTM, Flatten, Bidirectional
from keras.optimizers import Adam, SGD
from keras.layers.core import Activation
from keras.layers.normalization import BatchNormalization


def baseline(input_shape):
    input = Input(input_shape)
    lstm = LSTM(256, return_sequences=True)(input)
    lstm = LSTM(128)(lstm)
    dense = Dense(64)(lstm)
    
    dense = Dense(1)(dense)
    out = Activation('sigmoid')(dense) 
    
    model = Model(input, out)
    model.compile(optimizer=Adam(lr=1e-4), loss='binary_crossentropy', metrics=['accuracy'])
    
    return model


# bidirectional LSTM
def bidirectional_LSTM(input_shape):
    input = Input(input_shape)
    lstm = Bidirectional(LSTM(256, return_sequences=True))(input)
    lstm = Bidirectional(LSTM(128))(lstm)
    dense = Dense(64)(lstm)
    
    dense = Dense(1)(dense)
    out = Activation('sigmoid')(dense) 
    
    model = Model(input, out)
    model.compile(optimizer=Adam(lr=1e-4), loss='binary_crossentropy', metrics=['accuracy'])
    
    return model


def set_optimizer(model, lr=1e-4):
    model.compile(optimizer=Adam(lr=lr), loss='binary_crossentropy', metrics=['accuracy'])
    return model
